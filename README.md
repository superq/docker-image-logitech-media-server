<!--
SPDX-FileCopyrightText: 2014 JingleManSweep
SPDX-FileCopyrightText: 2015-2018 Lars Kellogg-Stedman <lars@oddbit.com>
SPDX-FileCopyrightText: 2019-2020 Mark Jonas <toertel@gmail.com>

SPDX-License-Identifier: MIT
-->

[![Pipeline status](https://gitlab.com/toertel/docker-image-logitech-media-server/badges/master/pipeline.svg)](https://gitlab.com/toertel/docker-image-logitech-media-server/pipelines)
[![REUSE status](https://reuse.software/badge/reuse-compliant.svg)](https://api.reuse.software/info/gitlab.com/toertel/docker-image-logitech-media-server)

# Docker Container for Logitech Media Server

This is a Docker image for running the Logitech Media Server package
(aka SqueezeboxServer) on amd64 (aka x86_64), arm32v7 (e.g. Raspberry Pi
2/3/4), and arm64v8.

The source code and automatic builds are maintained at
https://gitlab.com/toertel/docker-image-logitech-media-server.

Released images can be downloaded from
https://hub.docker.com/repository/docker/toertel/logitech-media-server.

The project is a fork of [larsks/docker-image-logitech-media-server](https://github.com/larsks/docker-image-logitech-media-server).

| :warning: Unfortunately the latest Squeezebox Radio firmware (7.7.3) comes with a bug which prevents it from connecting correctly to Logitech Media Server 8+. See https://github.com/Logitech/slimserver#sb-radio-and-logitech-media-server-8 for details and an easy mitigation. |
| --- |
## Run directly

```
docker run -d --init \
           -p 9000:9000 \
           -p 9090:9090 \
           -p 3483:3483 \
           -p 3483:3483/udp \
           -v /etc/localtime:/etc/localtime:ro \
           -v <local-state-dir>:/srv/squeezebox \
           -v <audio-dir>:/srv/music \
           toertel/logitech-media-server
```

The web interface runs on port 9000. If you also want this available
on port 80 (so you can use `http://yourserver/` without a port number
as the URL), you can add `-p 80:9000`, but you *must* also include `-p
9000:9000` because the players expect to be able to contact the server
on that port.

## Using docker-compose

There is a [docker-compose.yml](docker-compose.yml) included in this
repository that you will let you bring up a Logitech Media Server
container using `docker-compose`. The compose file includes the
following:

```
volumes:
    - ${AUDIO_DIR}:/srv/music
```

To provide a value for `AUDIO_DIR`, create a `.env`
file that points `AUDIO_DIR` at the location of your music library,
for example:

```
AUDIO_DIR=/home/USERNAME/Music
```

## Plugins

This image comes with prerequisites for some 3rd party plugins.

### Google Music

To enable the [Google Music Plugin](https://github.com/squeezebox-googlemusic/squeezebox-googlemusic/), add the plugin XML URL to the Plugins settings page. You will also need to follow the [Usage Instructions](https://github.com/squeezebox-googlemusic/squeezebox-googlemusic/#usage) to setup authentication.

    https://squeezebox-googlemusic.github.io/squeezebox-googlemusic/repository/repo.xml

## Image building process

### Register QEMU binfmt_misc handlers

Register `qemu-*-static` for all supported processors (except the
current one) as `binfmt_misc` handlers. You only need to do this once.

```
docker run --rm --privileged multiarch/qemu-user-static:register
```

In case you get an error message, try again with first removing all
registered `binfmt_misc` handlers.

```
docker run --rm --privileged multiarch/qemu-user-static:register --reset
```

### Prepare Build
To prepare the build download the required QEMU static user binaries.

```
./build/prepare.sh
```

> The build process uses Jinja2, specifically the
> [j2cli](https://pypi.org/project/j2cli/) command-line tool, for filling out a
> Dockerfile template. Most likely you will need to install *j2cli* using *pip*.
> It is recommended to install it in a
> [Python virtual environment](https://docs.python.org/3/tutorial/venv.html).

### Build Docker images
Docker images for all supported platforms are built one after the other.

```
./build/build-all.sh
```

### Test Docker image
You can run a suite of tests on an image. Pass the name of the image to
test.

```
./build/test.sh toertel/logitech-media-server:arm32v7-latest
```

### References
- [HomeOps - Building multi-arch docker images](https://lobradov.github.io/Building-docker-multiarch-images/)
- [How to Build ARM Docker Images on Intel host](https://hotblackrobotics.github.io/en/blog/2018/01/22/docker-images-arm/)
