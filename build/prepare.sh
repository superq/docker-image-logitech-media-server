#!/bin/bash

# SPDX-FileCopyrightText: 2020 Mark Jonas <toertel@gmail.com>
#
# SPDX-License-Identifier: MIT

QEMU_VERSION=v5.1.0-2

declare -A QEMU_ARCH
QEMU_ARCH[amd64]="x86_64"
QEMU_ARCH[arm32v7]="arm"
QEMU_ARCH[arm64v8]="aarch64"

# Download QEMU for given container architecture, else download all
if [ $# -eq 1 ]; then
    archs=${QEMU_ARCH[$1]}
    if [ -z "$archs" ]; then
        echo "Unsupported architecture: $1"
        exit 1
    fi
else
    archs=${QEMU_ARCH[*]}
fi

# Exit immediately if a command exits with a non-zero status
set -e

TMPDIR=$(mktemp -d -t qemu-static_XXXXXX)

cleanup() {
    rm -rf "${TMPDIR}"
}

trap cleanup EXIT

for target_arch in ${archs}; do
    wget -N -P "${TMPDIR}" "https://github.com/multiarch/qemu-user-static/releases/download/${QEMU_VERSION}/x86_64_qemu-${target_arch}-static.tar.gz"
    tar -xvf "${TMPDIR}/x86_64_qemu-${target_arch}-static.tar.gz"
done
