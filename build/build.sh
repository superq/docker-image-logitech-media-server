#!/bin/sh

# SPDX-FileCopyrightText: 2020 Mark Jonas <toertel@gmail.com>
#
# SPDX-License-Identifier: MIT

# Exit immediately if a command exits with a non-zero status
set -e

if [ "$#" -ne 3 ]; then
    echo "Usage: $0 REGISTRY_USER REGISTRY_REPO ARCHITECTURE" >&2
    exit 1
fi

CONTAINER_REGISTRY_USER="$1"
CONTAINER_REGISTRY_REPO="$2"
CONTAINER_ARCH="$3"

container_tag="${CONTAINER_REGISTRY_USER}/${CONTAINER_REGISTRY_REPO}:${CONTAINER_ARCH}-latest"
echo "+++ Build ${container_tag}"
BUILD_DATE=$(date -Iseconds)
export BUILD_DATE
IMAGE_VERSION=$(git describe --dirty)
export IMAGE_VERSION
COMMIT_SHA=${CI_COMMIT_SHA}
export COMMIT_SHA
echo "    BUILD_DATE=${BUILD_DATE}"
echo "    IMAGE_VERSION=${IMAGE_VERSION}"
echo "    COMMIT_SHA=${COMMIT_SHA}"
docker build --build-arg BUILD_DATE --build-arg IMAGE_VERSION --build-arg COMMIT_SHA -f "Dockerfile.${CONTAINER_ARCH}" -t "${container_tag}" .
