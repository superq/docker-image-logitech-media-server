#!/bin/sh

# SPDX-FileCopyrightText: 2020 Mark Jonas <toertel@gmail.com>
#
# SPDX-License-Identifier: MIT

CONTAINER_REGISTRY_USER=toertel
CONTAINER_REGISTRY_REPO=logitech-media-server
CONTAINER_ARCHS="amd64 arm32v7 arm64v8"

# Exit immediately if a command exits with a non-zero status
set -e

for container_arch in ${CONTAINER_ARCHS}; do
    ./build/create.sh "${container_arch}"
    ./build/build.sh "${CONTAINER_REGISTRY_USER}" "${CONTAINER_REGISTRY_REPO}" "${container_arch}"
done
