#!/bin/sh

# SPDX-FileCopyrightText: 2020 Mark Jonas <toertel@gmail.com>
#
# SPDX-License-Identifier: MIT

# Exit immediately if a command exits with a non-zero status
set -e

if [ "$#" -ne 1 ]; then
    echo "Usage: $0 ARCHITECTURE" >&2
    exit 1
fi

CONTAINER_ARCH="$1"

echo "+++ Create Dockerfile.${CONTAINER_ARCH}"
container_arch="${CONTAINER_ARCH}" j2 Dockerfile.j2 >"Dockerfile.${CONTAINER_ARCH}"
